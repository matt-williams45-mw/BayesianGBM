"""problems: 1)need to ensure commas are replaced with tabs- major problem with script
             2) need to et sep= working  """


"""Import"""
import pandas as pd
import numpy as np
from pandas import DataFrame
from numpy import genfromtxt, savetxt

"""directory"""
wd= "C://Users//simon_000//Desktop//Glioblastoma Project//"

patient1= wd +"nationwidechildrens.org_clinical_patient_gbm.txt"
"""problem, want to say sep= ','"""
df1=pd.read_csv(patient1, sep="\t")

"""delete columns"""

df2=df1.drop(['bcr_patient_uuid', 'history_lgg_dx_of_brain_tissue', 'birth_days_to', 'tumor_status', 'disease_code', 'histological_type', 'tumor_tissue_site', 'history_other_malignancy', 'history_neoadjuvant_treatment', 'method_initial_path_dx_other', 'ecog_score', 'days_to_initial_pathologic_diagnosis', 'prospective_collection', 'retrospective_collection', 'performance_status_timing', 'radiation_treatment_adjuvant', 'anatomic_neoplasm_subdivision', 'pharmaceutical_tx_adjuvant', 'treatment_outcome_first_course', 'new_tumor_event_dx_indicator', 'bcr_patient_barcode', 'form_completion_date', 'icd_10', 'icd_o_3_histology', 'icd_o_3_site', 'informed_consent_verified', 'patient_id', 'project_code', 'tissue_source_site', 'form_completion_date'], 1)
df2.to_csv(wd + "gbedit1.csv")

""" transpose data"""

data=np.transpose(df2)
data.to_csv(wd +"gbedit2.csv")

"""knock off columns 2 and 3"""
df3=data.drop([0, 1], 1)
df3.to_csv(wd + "gbedit3.csv")

"""remove commas, replace with tabs- this bit isn't working, the rest is"""
df4=df3
df4.to_csv(wd +"gbedit4.csv")

"""remove colons"""
df5=df4.replace({":": ""}, regex=True)
df5.to_csv(wd + "gbedit5.csv")


"""remove NA, NP, NE"""
df6=df5.replace({"[Not Available]": "-1"})
df7=df6.replace({"[Not Applicable]": "-1"})
df8=df7.replace({"[Not Evaluated]": "-1"})
df9=df8.replace({"[Unknown]": "-1"})
df10=df9.to_csv(wd + "gbedit6.csv", sep= "\t")

df11=df9.replace({"NOT HISPANIC OR LATINO": "NotHispaniicOrLatino"})
df12=df11.replace({"Tumor resection": "Tumorresection"})
df13=df12.replace({"BLACK OR AFRICAN AMERICAN": "BlackorAfricanAmerican"})
df14=df13.to_csv(wd +"gbedit7.csv", sep= "\t")

"""insert regulators, continuous, discrete"""

f = open(wd + "gbedit7.csv", 'r')
temp = f.read()
f.close()

f = open(wd + "gbedit7.csv", 'w')
f.write("#regulators  age_at_initial_pathologic_diagnosis last_contact_days_to initial_pathologic_dx_year karnofsky_score vital_status method_initial_path_dx ethnicity race gender\n#continuous death_days_to age_at_initial_pathologic_diagnosis last_contact_days_to initial_pathologic_dx_year karnofsky_score\n#discrete  method_initial_path_dx	Tumor resection ethnicity race gender\nconditions")
f.write(temp)
f.close()



"""to run bnfinder from command line"""
"""bnf -e gbedit6.csv -n output1.sif -v -t output1.txt"""
